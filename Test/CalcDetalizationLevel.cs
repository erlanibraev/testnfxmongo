﻿using System;
using NFX;
using NUnit.Framework;
using TestNFXMongo;

namespace Test
{
  [TestFixture]
  public class CalcDetalizationLevel
  {
    [TestCase("2013-01-01 13:00:00", "2014-01-01 13:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-02-01 13:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-01-01 14:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-01-01 13:45:00")]
    public void Minus(string startDate, string endDate)
    {
      var sd = startDate.AsDateTime();
      var ed = endDate.AsDateTime();
      var minus = ed - sd;
      Console.WriteLine(minus);
    }

    [TestCase("2013-01-01 13:00:00", "2014-01-01 13:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-02-01 13:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-01-01 14:00:00")]
    [TestCase("2013-01-01 13:00:00", "2013-01-01 13:45:00")]
    [TestCase("2013-01-01 13:00:00", "2013-01-02 13:00:00")]
    public void CalcDetalizarionLevelMonth(string startDate, string endDate)
    {
      var sd = startDate.AsDateTime();
      var ed = endDate.AsDateTime();
      var minus = ed - sd;
      var result = VolumeDetalizationLevel.Monthly;
      if (minus.Days == 0)
        result = VolumeDetalizationLevel.Fractional;
      else if (minus.Days < 3)
        result = VolumeDetalizationLevel.Hourly;
      else if (minus.Days < 30)
        result = VolumeDetalizationLevel.Daily;
      else if (minus.Days < 90)
        result = VolumeDetalizationLevel.Weekly;
      else
        result = VolumeDetalizationLevel.Monthly;

/*
      if (minus.Days < 90)
      {
        result = VolumeDetalizationLevel.Weekly;
        if (minus.Days < 30)
        {
          result = VolumeDetalizationLevel.Daily;
          if (minus.Days < 3)
          {
            result = VolumeDetalizationLevel.Hourly;
            if (minus.Days == 0)
              result = VolumeDetalizationLevel.Fractional;
          }
        }
      }
*/

      Console.WriteLine("{0} - {1} : {2}".Args(startDate, endDate, result.ToString()));
    }
  }
}