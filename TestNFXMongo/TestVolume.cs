﻿using System;
using System.Collections.Generic;
using NFX;
using NFX.DataAccess.Distributed;
using NFX.DataAccess.MongoDB.Connector;
using NFX.Serialization.BSON;
using NFX.Serialization.JSON;

namespace TestNFXMongo
{
  public enum VolumeDetalizationLevel
  {
    /// <summary>
    /// YYYY MM
    /// </summary>
    Monthly, 
    /// <summary>
    /// YYYY MM
    /// </summary>
    Weekly,
    /// <summary>
    /// YYYY MM DD
    /// </summary>
    Daily,
    /// <summary>
    /// YYYY MM DD HH
    /// </summary>
    Hourly,
    /// <summary>
    /// YYYY MM DD HH F
    /// </summary>
    Fractional

  }
  
  public class TestVolume
  {

    public const string FIELD_G_ENTITY = "gent";
    public const string FIELD_G_SHARD = "gshr";
    public const string FIELD_VALUE = "val";
    public const string FIELD_COUNT = "cnt";
    public const string FIELD_DATETIME = "dt";
    public const string FIELD_VALUE_UNIT_NAME = "vun";
    public const string FIELD_DIM_PREFIX = "d-";
    
    public static DateTime RoundDate(DateTime date, VolumeDetalizationLevel level)
    {
      switch (level)
      {
        case VolumeDetalizationLevel.Monthly:
          return new DateTime(date.Year, date.Month, day: 1, hour: 13, minute: 0, second: 0, kind: DateTimeKind.Utc);
        case VolumeDetalizationLevel.Weekly:
          DateTime result = date;
          while (result.DayOfWeek != System.DayOfWeek.Monday) result = result.AddDays(-1);
          return new DateTime(result.Year, result.Month, result.Day, hour: 13, minute: 0, second: 0,
            kind: DateTimeKind.Utc);
        case VolumeDetalizationLevel.Daily:
          return new DateTime(date.Year, date.Month, date.Day, hour: 13, minute: 0, second: 0, kind: DateTimeKind.Utc);
        case VolumeDetalizationLevel.Hourly:
          return new DateTime(date.Year, date.Month, date.Day, date.Hour, minute: 0, second: 0, kind: DateTimeKind.Utc);
        case VolumeDetalizationLevel.Fractional:
          return new DateTime(date.Year, date.Month, date.Day, date.Hour, minute: 0, second: 0,
            kind: DateTimeKind.Utc); //todo Fractional
        default:
          throw new Exception();
      }
    }

    public TestVolume(VolumeDetalizationLevel detalizationLevel, ServerNode server, string[] Entities)
    {
      m_DetalizationLevel = detalizationLevel;
      m_Server = server;
      foreach (var entity in Entities)
      {
        m_UpdateEntries[entity] = new TrendingUpdateEntry(entity, new string[] {"test1", "test2", "test3"});
      }
      
    }

    private readonly VolumeDetalizationLevel m_DetalizationLevel;
    private readonly ServerNode m_Server;
    private Dictionary<string, TrendingUpdateEntry> m_UpdateEntries = new Dictionary<string, TrendingUpdateEntry>();
    
    public void writeDoc(string table, TestData gauge)
    {
      var collection = m_Server[m_DetalizationLevel.ToString()][gauge.Entity];
      var date = RoundDate(App.TimeSource.UTCNow, m_DetalizationLevel);
      var updateEntry = m_UpdateEntries[gauge.Entity];

      updateEntry.G_Entity = gauge.gent;
      updateEntry.G_Shard = gauge.gshr;
      updateEntry.DateTime = date;
      updateEntry.Value = gauge.val;
      updateEntry.Count = gauge.Count;
      updateEntry.Dimensions = gauge.Dimensions;
      
      Console.WriteLine(updateEntry.UpdateEntry.Query.ToString());
      Console.WriteLine(updateEntry.UpdateEntry.Update.ToString());

      var r = collection.Update(updateEntry.UpdateEntry);
      // Console.WriteLine(r.TotalDocumentsAffected);
    }
    
    public TestData[] ReadData (string table, Query query)
    {
      var result = new List<TestData>();
      var collection = m_Server[m_DetalizationLevel.ToString()]["entity1"];
      var sort = new BSONDocument().Set(new BSONInt32Element(FIELD_VALUE, -1));
      var find = new Query();
      find.Set(new BSONDocumentElement("$query", query));
      find.Set(new BSONDocumentElement("$orderby", sort));
      var cursor = collection.Find(find);

      try
      {
        foreach (var doc in cursor)
        {
          var td = new TestData();
          td.Entity = "test";
          td.gent = RowConverter.GDID_BSONtoCLR((BSONBinaryElement) doc[FIELD_G_ENTITY]);
          td.gshr = RowConverter.GDID_BSONtoCLR((BSONBinaryElement) doc[FIELD_G_SHARD]);
          td.dt = doc[FIELD_DATETIME].ObjectValue.AsDateTime();
          td.val = doc[FIELD_VALUE].ObjectValue.AsLong();
          // td.Count = doc[FIELD_COUNT].ObjectValue.AsLong();
          td.Dimensions = null;
          
          result.Add(td);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
      
      return result.ToArray();
    }
    
  }

  public class TestData
  {
    public string Entity { get; set; }
    public GDID gent { get; set; }
    public GDID gshr { get; set; }
    public DateTime? dt { get; set; }
    public long val { get; set; }
    public long Count { get; set; }
    public IEnumerable<KeyValuePair<string, string>> Dimensions { get; set; }
  }
}