﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFX;
using NFX.DataAccess.Distributed;
using NFX.DataAccess.MongoDB.Connector;
using NFX.RelationalModel;
using NFX.Serialization.BSON;

namespace TestNFXMongo
{
  public sealed class TrendingUpdateEntry
  {
    
    public TrendingUpdateEntry(string entityType, string[] dimensionNames)
    {
      m_EntityType = entityType;
      
      m_Filter.Set(m_G_Entity);
      m_Filter.Set(m_G_Shard);
      m_Filter.Set(m_DateTime);

      m_Doc.Set(m_G_Entity);
      m_Doc.Set(m_G_Shard);
      m_Doc.Set(m_DateTime);


      m_Inc.Set(m_Value);
      m_Inc.Set(m_Count);

      m_IncDoc.Set(new BSONDocumentElement("$setOnInsert", m_Doc));
      m_IncDoc.Set(new BSONDocumentElement("$inc", m_Inc));


      foreach (var dimensionName in dimensionNames)
      {
        var element = new BSONStringElement(dimensionName, "");
        var kvp = new KeyValuePair<string, BSONStringElement>(StringConsts.FIELD_DIM_PREFIX+dimensionName, element);
        m_Dimensions.Add(kvp);
        m_Doc.Set(element);
        m_Filter.Set(element);
      }
      
      m_UpdateEntry = new UpdateEntry(m_Filter, m_IncDoc, false, true);
    }
    
    private readonly string m_EntityType;
    private readonly UpdateEntry m_UpdateEntry;
    
    private readonly BSONDocument m_Filter = new BSONDocument();
    private readonly BSONDocument m_Doc = new BSONDocument();
    private readonly BSONDocument m_IncDoc = new BSONDocument();
    private readonly BSONDocument m_Inc = new BSONDocument();
    
    private readonly BSONBinaryElement m_G_Entity = new BSONBinaryElement(StringConsts.FIELD_G_ENTITY, new BSONBinary());
    private readonly BSONBinaryElement m_G_Shard = new BSONBinaryElement(StringConsts.FIELD_G_SHARD, new BSONBinary());
    private readonly BSONDateTimeElement m_DateTime = new BSONDateTimeElement(StringConsts.FIELD_DATETIME, DateTime.MinValue);
    private readonly List<KeyValuePair<string, BSONStringElement>> m_Dimensions = new  List<KeyValuePair<string, BSONStringElement>>();
    
    private readonly BSONInt64Element m_Value = new BSONInt64Element(StringConsts.FIELD_VALUE, 0);
    private readonly BSONInt64Element m_Count = new BSONInt64Element(StringConsts.FIELD_VALUE, 0);

    
    public string EntityType { get{return m_EntityType;} }
    
    public UpdateEntry UpdateEntry { get{return m_UpdateEntry;} }

    public GDID G_Entity 
    {
      get { return RowConverter.GDID_BSONtoCLR(m_G_Entity); }
      set { m_G_Entity.Value = new BSONBinary(BSONBinaryType.UserDefined, value.Bytes); }
    }

    public GDID G_Shard 
    {
      get { return RowConverter.GDID_BSONtoCLR(m_G_Shard); }
      set { m_G_Shard.Value = new BSONBinary(BSONBinaryType.UserDefined, value.Bytes); }
    }

    public DateTime DateTime
    {
      get { return m_DateTime.ObjectValue.AsDateTime(); }
      set { m_DateTime.Value = value; }
    }
    
    public long Value {
      get { return m_Value.ObjectValue.AsLong(); }
      set { m_Value.Value = value; }
    }

    public long Count {
      get { return m_Count.ObjectValue.AsLong(); }
      set { m_Count.Value = value; }
    }

    public IEnumerable<KeyValuePair<string, string>> Dimensions
    {
      get
      {
        var result = new List<KeyValuePair<string, string>>();
        foreach (var item in m_Dimensions)
        {
          var element = item.Value;
          var key = item.Key;
          var kvp = new KeyValuePair<string, string>(key, element.ObjectValue.ToString());
          result.Add(kvp);
        }
        return result;
      }
      set
      {
        foreach (var dimKVP in m_Dimensions)
        {
          var kvp = value.FirstOrDefault(pair => (StringConsts.FIELD_DIM_PREFIX+pair.Key).EqualsOrdIgnoreCase(dimKVP.Key));
          dimKVP.Value.Value = kvp.Value ?? "";
          if (kvp.Value == null)
          {
            m_Filter.Delete(dimKVP.Key);
            m_Doc.Delete(dimKVP.Key);
          }
          else if(m_Filter[dimKVP.Key] != null)
          {
            m_Filter.Set(dimKVP.Value);
            m_Doc.Set(dimKVP.Value);
          }
        }
      }
      
    }

  }
}