﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using NFX;
using NFX.DataAccess.Distributed;
using NFX.DataAccess.MongoDB.Connector;
using NFX.Serialization.BSON;
using NFX.Serialization.JSON;

namespace TestNFXMongo
{
  public class Program
  {
    public static void Main(string[] args)
    {
      using (var client = new MongoClient("Test"))
      {
        var volume = new TestVolume(VolumeDetalizationLevel.Daily, client.DefaultLocalServer, new [] {"entity1", "entity2"});
        var gdid = new GDID();

        var test1 = new KeyValuePair<string, string>("test1", string.Empty);
        var test2 = new KeyValuePair<string, string>("test2", string.Empty);
        var test3 = new KeyValuePair<string, string>("test3", string.Empty);

        for (int i = 0; i < 10; i++)
        {
          var gauge = new TestData()
          {
            Entity = "entity1",
            gent =gdid,
            gshr = gdid,
            dt = DateTime.MinValue,
            val = 3,
            Count = 1,
            
          };

          var dim = new List<KeyValuePair<string, string>>();
          dim.Add(new KeyValuePair<string, string>("test1", "test1"));
          dim.Add(new KeyValuePair<string, string>("test2", "test2"));
          if ( i < 5) dim.Add(new KeyValuePair<string, string>("test3", "tets3"));

          gauge.Dimensions = dim;
                      
          volume.writeDoc("TestDb", gauge);
        }
        
        Console.WriteLine(" ------------- ");
        
        var query = new Query();
        var betweenDate = new BSONDocument();
        var utcNow = DateTime.UtcNow.AddDays(-1);
        betweenDate.Set(new BSONDateTimeElement("$gte", new DateTime(utcNow.Year, utcNow.Month, utcNow.Day, 0, 0, 0)));
        betweenDate.Set(new BSONDateTimeElement("$lte", new DateTime(utcNow.AddDays(1).Year, utcNow.AddDays(1).Month, utcNow.AddDays(1).Day, 0, 0, 0)));
        query.Set(new BSONDocumentElement("dt", betweenDate));
        volume.ReadData("TestDb", query)
          .ForEach((data, i) =>
          {
            Console.Write(data.gent);
            Console.Write(";");
            Console.Write(data.val);
            Console.Write(";");
            Console.Write(data.dt);
            Console.WriteLine(";");
          });
        
        Console.ReadLine();
      }  
    }

    public static void printDb(MongoClient client)
    {
      foreach (var db in client.DefaultLocalServer.Databases)
      {
        Console.WriteLine(db.Name);
      }
      
    }
    
  }
}