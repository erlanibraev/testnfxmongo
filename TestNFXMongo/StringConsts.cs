﻿using System;

namespace TestNFXMongo
{
  public class StringConsts
  {
    public const string ARGUMENT_ERROR = "Argument error: ";
    public const string DATABASE_NOT_CONFIGURED_ERROR = "Database not configured";
    
    
    public const string FIELD_G_ENTITY = "gent";
    public const string FIELD_G_SHARD = "gshr";
    public const string FIELD_VALUE = "val";
    public const string FIELD_COUNT = "cnt";
    public const string FIELD_DATETIME = "dt";
    public const string FIELD_VALUE_UNIT_NAME = "vun";
    public const string FIELD_DIM_PREFIX = "d-";
    
  }
}